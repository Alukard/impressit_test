const Books = require('../models/books');
const uuid = require('uuid');

async function list() {
    return await Books.scan().exec();
}

async function getById(req) {
    return await Books.get({uuid: req.params.uuid});
}

async function create(req) {
    const id = uuid.v4();
    const body = req.body;

    return await Books.create({
        uuid: id,
        name : body.name,
        releaseDate : new Date(body.releaseDate),
        authorName : body.authorName
    });
}

async function update(req) {
    const body = req.body;
    const book = await Books.get({uuid: req.params.uuid});

    // should be validate in validator
    delete body.uuid;

    body.releaseDate = new Date(body.releaseDate);

    return await Books.update({...book, ...body});
}

async function remove(req) {
    return await Books.delete({uuid: req.params.uuid});
}

module.exports = {
    list,
    getById,
    create,
    update,
    remove
};