const dynamoose = require('dynamoose');

const Schema = dynamoose.Schema;

const booksSchema = new Schema ({
    uuid: {
        type: String,
        required: true,
        unique: true,
        hashKey: true
    },
    name: {
        type: String
    },
    releaseDate: {
        type: Date
    },
    authorName: {
        type: String
    }
});

module.exports = dynamoose.model('Books', booksSchema);
