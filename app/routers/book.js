const express = require('express');
const router = express.Router();
const bookCtrl = require('../controllers/book');
const handler = require('../helpers/handler').handler;

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now());
    next();
});

// all routers should be moved to serverless.yml file, without "proxy ALL"

router.get('/books', handler(bookCtrl.list));
router.get('/book/:uuid',handler( bookCtrl.getById));

router.post('/book/add', handler(bookCtrl.create));
router.post('/book/:uuid/update', handler(bookCtrl.update));
router.post('/book/:uuid/delete', handler(bookCtrl.remove));

module.exports = router;

