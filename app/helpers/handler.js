function handler(fn) {
    return function(req, res) {
        return fn(req).then((data) => {
            return res.json(data);
        }).catch((e) => {
            console.log(e);

            return res.status(500).json('Server error');
        });
    };
}

module.exports = { handler };