const dynamoose = require('dynamoose');
const config = require('../configs/config');
const nodeEnv = process.env.NODE_ENV || 'local';

if(config[nodeEnv] && config[nodeEnv].dynamoDB) {
    const ddb = new dynamoose.aws.sdk.DynamoDB(config[nodeEnv].dynamoDB);

    dynamoose.aws.ddb.set(ddb);
}
