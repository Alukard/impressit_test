need to update serverless.yml file (DB settings) and use DB variables like table name in code

global dependencies:
    nodeJs 10+
    npm
    dynamoDB
    serverless

start project:
    npm i
    serverless invoke local --function books

commands:
    npm run test
    npm run lint


create request body example:
    {"name":"added book111","releaseDate":"2025-01-10T14:01:15.847Z","authorName":"added author name1111"}