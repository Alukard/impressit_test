require('dotenv').config();

const serverless = require('serverless-http');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const bookRouter = require('./app/routers/book');

require('./app/helpers/initDB');

app.use(bodyParser.json());

// routers
app.use(bookRouter);


app.use(function(err, req, res, next) {
    console.log(err);

    res.status(500).send('Server error');

    next();
});


module.exports.handler = serverless(app);