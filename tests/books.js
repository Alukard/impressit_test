// should be moved to general test file
require('../app/helpers/initDB');

const chai = require('chai');
const Books = require('../app/models/books');
const BookCtrl = require('../app/controllers/book');
const sinon = require('sinon');

const expect = chai.expect;

// can be moved to json file
const stubData = {
    get: {
        uuid: 'testUUID',
        name : 'book name',
        releaseDate : '2020-05-10T14:01:15.847Z',
        authorName : 'test author name'
    }
};

describe('Book', async () => {
    let stubs;

    before(() => {
        stubs = [
            sinon.stub(Books, 'get').returns(stubData.get),
        ];
    });

    after(() => {
        stubs.forEach((s) => s.restore());
    });


    it('should return book by id', async () => {
        const req = {
            params: {uuid: 'testUUID'}
        };

        const book = await BookCtrl.getById(req);

        expect(book).to.eql(stubData.get);
    });
});
